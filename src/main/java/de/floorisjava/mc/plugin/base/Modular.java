package de.floorisjava.mc.plugin.base;

import de.floorisjava.mc.plugin.base.detail.EmergencyListener;
import de.floorisjava.mc.plugin.base.util.EntryPoint;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Main entry point.
 */
@EntryPoint
public class Modular extends JavaPlugin {

    /**
     * The plugin's instance.
     */
    private static Modular instance;

    /**
     * Whether emergency mode is enabled.
     */
    private boolean emergencyEnabled;

    /**
     * Triggers emergency mode.
     *
     * @param cause The causing throwable. May be {@code null} if there is none.
     * @see #requireNoexcept(Runnable)
     */
    public static void triggerEmergencyMode(final Throwable cause) {
        instance.getLogger().severe("Emergency condition triggered. Printing trace...");
        if (cause != null) {
            cause.printStackTrace();
        } else {
            new Throwable().printStackTrace();
        }

        if (!instance.emergencyEnabled) {
            instance.getServer().getPluginManager().registerEvents(new EmergencyListener(), instance);
            instance.getLogger().severe("Initial installation of EmergencyListener complete!");
            instance.emergencyEnabled = true;
        }
    }

    /**
     * Runs the given task. Any uncaught exception leaving the task leads to an emergency condition.
     *
     * @param task The task.
     * @see #triggerEmergencyMode(Throwable)
     */
    public static void requireNoexcept(final Runnable task) {
        try {
            task.run();
        } catch (final Throwable throwable) {
            triggerEmergencyMode(throwable);
        }
    }

    @Override
    public void onEnable() {
        instance = this;
    }
}
