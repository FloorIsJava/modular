/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mc.plugin.base.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/**
 * Represents a leaf node in a command tree.
 */
public abstract class LeafCommand extends TreeCommand {

    /**
     * The permission that is required for this command node.
     */
    private final String permission;

    /**
     * Constructor.
     *
     * @param helpLine The help line.
     */
    protected LeafCommand(final String helpLine, final String permission) {
        super(helpLine);
        this.permission = permission;
    }

    /**
     * Invokes this command node.
     *
     * @param sender The command sender.
     * @param args   The command arguments.
     */
    protected abstract void invoke(final CommandSender sender, final String[] args);

    @Override
    public final boolean onCommand(final CommandSender sender, final Command command, final String label,
                                   final String[] args) {
        if (permission != null) {
            if (!sender.hasPermission(permission)) {
                sender.sendMessage("Missing permission: " + permission);
                return true;
            }
        }

        // Invoke the command.
        invoke(sender, args);

        return true;
    }
}
