/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mc.plugin.base.config;

import lombok.Getter;
import lombok.SneakyThrows;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

/**
 * Wrapper for {@link YamlConfiguration}s together with their backing {@link File}s.
 */
public class ConfigurationFile {

    /**
     * The file backing the configuration.
     */
    private final File backendFile;

    /**
     * The configuration in the file.
     */
    private YamlConfiguration config;

    /**
     * Whether the configuration file should automatically be saved. Recommended for files not changed by the user.
     */
    @Getter
    private final boolean autoSave;

    /**
     * Constructor.
     *
     * @param file The file backing the configuration.
     */
    @SneakyThrows
    public ConfigurationFile(final File file, final boolean autoSave) {
        if (!file.exists()) {
            if (!file.createNewFile()) {
                throw new IllegalStateException("file does already exist");
            }
        }

        backendFile = file;
        this.autoSave = autoSave;
    }

    /**
     * Saves the configuration file.
     */
    @SneakyThrows
    public void save() {
        if (config != null) {
            config.save(backendFile);
        }
    }

    /**
     * Reloads the configuration file.
     */
    public void reload() {
        if (config != null) {
            load();
        }
    }

    /**
     * Obtains the configuration.
     *
     * @return The configuration.
     */
    public ConfigurationSection getConfig() {
        if (config == null) {
            load();
        }
        return config;
    }

    /**
     * Loads the configuration.
     */
    private void load() {
        config = YamlConfiguration.loadConfiguration(backendFile);
    }
}
