/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mc.plugin.base.config;

import lombok.RequiredArgsConstructor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.Plugin;

/**
 * Provides namespaced configuration for a module.
 */
@RequiredArgsConstructor
public class ModuleConfigurationProvider implements ConfigurationProvider {

    /**
     * The plugin that owns the configuration.
     */
    private final Plugin plugin;

    /**
     * The namespace that is provided.
     */
    private final String namespace;

    @Override
    public ConfigurationSection get() {
        return plugin.getConfig().getConfigurationSection(namespace);
    }
}
