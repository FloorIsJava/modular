/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mc.plugin.base.detail;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;

/**
 * Prevents server usage altogether, for cases when normal operation cannot be established.
 */
public class EmergencyListener implements Listener {

    @EventHandler
    public void onPlayerLogin(final PlayerLoginEvent event) {
        if (!event.getPlayer().isOp()) {
            event.disallow(PlayerLoginEvent.Result.KICK_WHITELIST, "Unable to join: Internal Server Error");
        }
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        event.getPlayer().sendMessage(ChatColor.DARK_RED + "The server is in a critical condition due to plugin errors.");
    }
}
