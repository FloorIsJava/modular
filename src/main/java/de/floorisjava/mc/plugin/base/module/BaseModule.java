/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mc.plugin.base.module;

import de.floorisjava.mc.plugin.base.config.ConfigurationFile;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.logging.Logger;

/**
 * Provides basic module functionality.
 */
@RequiredArgsConstructor
public abstract class BaseModule implements Module {
    /**
     * The module manager.
     */
    @Getter
    private final ModuleManager moduleManager;

    /**
     * The list of task IDs owned by the module.
     */
    private List<Integer> taskIds;

    /**
     * The registered listener providers for the module.
     */
    private List<Supplier<? extends Listener>> listenerProviders;

    /**
     * The currently registered listeners.
     */
    private List<Listener> listeners;

    /**
     * A map of configuration files.
     */
    private Map<String, ConfigurationFile> configurationFiles;

    /**
     * Whether the module is started.
     */
    @Getter
    private boolean started = false;

    @Override
    public final void start() {
        logger().info("Starting module " + getClass().getSimpleName());

        // Allow the user to start the module before registering listeners, as dependencies may not exist yet otherwise.
        startModule();

        if (listenerProviders != null) {
            listeners = new ArrayList<>();
            listenerProviders.forEach(p -> {
                final Listener listener = p.get();
                listeners.add(listener);
                Bukkit.getPluginManager().registerEvents(listener, moduleManager);
            });
        }

        logger().info("Started module " + getClass().getSimpleName());
        started = true;
    }

    @Override
    public final void stop() {
        started = false;
        logger().info("Stopping module " + getClass().getSimpleName());
        stopModule();

        if (taskIds != null) {
            taskIds.forEach(Bukkit.getScheduler()::cancelTask);
        }
        taskIds = null;

        if (listeners != null) {
            listeners.forEach(HandlerList::unregisterAll);
        }
        listeners = null;
        listenerProviders = null;

        if (configurationFiles != null) {
            configurationFiles.values().stream()
                    .filter(ConfigurationFile::isAutoSave)
                    .forEach(ConfigurationFile::save);
        }
        configurationFiles = null;
        logger().info("Stopped module " + getClass().getSimpleName());
    }

    /**
     * Starts the module.
     * <p>
     * At this point:
     * <ul>
     *     <li>Listeners are not yet registered.</li>
     *     <li>The module is not yet marked as started.</li>
     * </ul>
     */
    protected void startModule() {
    }

    /**
     * Stops the module.
     * <p>
     * At this point:
     * <ul>
     *     <li>The module is already marked as stopped.</li>
     *     <li>Tasks are not yet cancelled.</li>
     *     <li>Listeners are still registered.</li>
     *     <li>Auto-save configuration files are not yet saved.</li>
     * </ul>
     */
    protected void stopModule() {
    }

    /**
     * Registers a task with the module. This is optional. Pending tasks will be cancelled on module stop.
     *
     * @param taskId The task id.
     */
    protected void addTask(final int taskId) {
        if (taskIds == null) {
            taskIds = new ArrayList<>();
        }
        taskIds.add(taskId);
    }

    /**
     * Adds a listener to be registered while the module is active. When the provider is executed,
     * {@link #startModule()} is guaranteed to have been completed.
     *
     * @param provider A provider for the listener.
     */
    protected void addListener(final Supplier<? extends Listener> provider) {
        if (listenerProviders == null) {
            listenerProviders = new ArrayList<>();
        }
        listenerProviders.add(provider);
    }

    /**
     * Copys the default configuration for the given file, if the file does not yet exist.
     *
     * @param file The file.
     */
    protected void copyDefaultConfiguration(final String file) {
        final File fileObject = new File(moduleManager.getDataFolder(), file);
        if (!fileObject.exists()) {
            getModuleManager().saveResource(file, false);
        }
    }

    /**
     * Obtains a configuration for the given filename.
     *
     * @param file     The filename.
     * @param autoSave Whether the configuration file should be automatically saved on stop.
     * @return The configuration.
     */
    protected ConfigurationFile getConfiguration(final String file, final boolean autoSave) {
        if (configurationFiles == null) {
            configurationFiles = new HashMap<>();
        }

        return configurationFiles.computeIfAbsent(file, k -> {
            final File fileObject = new File(moduleManager.getDataFolder(), file);
            fileObject.getParentFile().mkdirs();

            return new ConfigurationFile(fileObject, autoSave);
        });
    }

    /**
     * Returns a logger for the module..
     *
     * @return The logger.
     */
    protected Logger logger() {
        return moduleManager.getLogger();
    }
}
