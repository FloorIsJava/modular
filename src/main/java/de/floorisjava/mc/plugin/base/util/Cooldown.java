/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mc.plugin.base.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Provides cooldowns for a collection of objects.
 *
 * @param <T> The object type.
 */
public class Cooldown<T> {

    /**
     * The cooldown storage.
     */
    private final Map<T, Long> cooldowns = new HashMap<>();

    /**
     * Checks whether the object is on cooldown.
     *
     * @param t The object.
     * @return {@code true} if the object is on cooldown.
     */
    public boolean isOnCooldown(final T t) {
        final Long expiry = cooldowns.get(t);
        return expiry != null && expiry > System.currentTimeMillis();
    }

    /**
     * Obtains the remaining cooldown time, in milliseconds.
     *
     * @param t The object.
     * @return The remaining cooldown time.
     */
    public long getRemainingCooldown(final T t) {
        final Long expiry = cooldowns.get(t);
        return expiry == null ? 0 : Math.max(0, expiry - System.currentTimeMillis());
    }

    /**
     * Adds the given amount of milliseconds to the cooldown.
     *
     * @param t      The object.
     * @param amount The amount of milliseconds.
     */
    public void addCooldown(final T t, final long amount) {
        if (amount <= 0) {
            return;
        }

        if (isOnCooldown(t)) {
            cooldowns.put(t, cooldowns.get(t) + amount);
        } else {
            cooldowns.put(t, System.currentTimeMillis() + amount);
        }
    }
}
