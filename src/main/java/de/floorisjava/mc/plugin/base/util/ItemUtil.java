/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mc.plugin.base.util;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Provides utilities for managing items.
 */
public final class ItemUtil {

    /**
     * Consumes a single item from a stack.
     *
     * @param item The item.
     */
    public static void consumeOne(final ItemStack item) {
        if (item == null || item.getType() == Material.AIR) {
            return;
        }

        if (item.getAmount() == 1) {
            item.setType(Material.AIR);
        } else {
            item.setAmount(item.getAmount() - 1);
        }
    }

    /**
     * Damages the given item.
     *
     * @param item   The item.
     * @param damage The amount of damage to inflict.
     * @throws IllegalArgumentException If the item is not damageable.
     */
    public static void damageItem(final ItemStack item, final int damage) {
        if (item == null || item.getType() == Material.AIR) {
            return;
        }

        final ItemMeta meta = item.getItemMeta();
        if (meta instanceof Damageable) {
            final int newDamage = ((Damageable) meta).getDamage() + damage;
            if (newDamage > item.getType().getMaxDurability()) {
                item.setType(Material.AIR);
            } else {
                ((Damageable) meta).setDamage(newDamage);
                item.setItemMeta(meta);
            }
        } else {
            throw new IllegalArgumentException("not damageable!");
        }
    }
}
