/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mc.plugin.base.util;

import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;

import java.util.Objects;

/**
 * Provides utilities for managing players.
 */
public final class PlayerUtil {

    /**
     * Heals the given player by the given amount.
     *
     * @param who    The player.
     * @param amount The amount.
     */
    public static void heal(final Player who, final double amount) {
        final double maxHealth = Objects.requireNonNull(who.getAttribute(Attribute.GENERIC_MAX_HEALTH)).getValue();
        who.setHealth(Math.min(maxHealth, who.getHealth() + amount));
    }

    /**
     * Takes food levels from the given player.
     *
     * @param who    The player.
     * @param levels The amount of food levels.
     */
    public static void takeFood(final Player who, final int levels) {
        who.setFoodLevel(Math.max(0, who.getFoodLevel() - levels));
    }
}
